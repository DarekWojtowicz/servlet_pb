<%--
  Created by IntelliJ IDEA.
  User: pbrzozowski
  Date: 2019-09-25
  Time: 22:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome site!</title>
</head>
<body>
<%
    out.println("<p>Welcome!</p>");
    out.println("<p>name " + request.getAttribute("name")+"</p>");
    out.println("<p>surname " + request.getAttribute("lastName")+"</p>");
%>
</body>
</html>
