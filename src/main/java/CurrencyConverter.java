import java.util.function.Function;
import java.util.stream.Stream;

public enum CurrencyConverter {
    PLN_TO_EUR("PLN", "EUR", (Float amountToConvert) -> {return amountToConvert / 4.35f;}),
    PLN_TO_USD("PLN", "USD", (Float amountToConvert) -> {return amountToConvert / 4.0f;}),
    UNKNOWN("", "", (Float amountToConvert) -> { return amountToConvert;});

    private String input;
    private String output;
    private Function<Float, Float> convertFunction;

    CurrencyConverter(String input, String output, Function<Float, Float> convertFunction) {
        this.input = input;
        this.output = output;
        this.convertFunction = convertFunction;
    }

    public static float convertCurrency(String input, String output, float amountToConvert){

        CurrencyConverter cash = Stream.of(values())
                .filter((CurrencyConverter converter) -> {
                    return converter.input.equals(input) &&
                            converter.output.equals(output);
                })
                .findAny().orElse(CurrencyConverter.UNKNOWN);

        return cash.convertFunction.apply(amountToConvert);
    }
}
