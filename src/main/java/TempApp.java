import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/temp")//->http://localhost:8080/temp

public class TempApp extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        writer.println("Lista ksiazek: ");

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int temp = Integer.valueOf(req.getParameter("temperature"));
        String inputUnit = req.getParameter("input");
        String outputUnit = req.getParameter("output");
        PrintWriter out = resp.getWriter();
        int output;
        out.println("Przeliczam: ");
        if(inputUnit.equals("C") && outputUnit.equals("K")){
            output = temp + 273;
            out.println(temp + " " + inputUnit + " wynosi: " + output + " " + outputUnit);
        }
        if(inputUnit.equals("K") && outputUnit.equals("C")){
            output = temp - 273;
            out.println(temp + " " + inputUnit + " wynosi: " + output  + " " + outputUnit);
        }
        if(inputUnit.equals("C") && outputUnit.equals("F")){
            output = temp * 9 / 5 + 32;
            out.println(temp + " " + inputUnit + " wynosi: " + output + " " + outputUnit);
        }
        if(inputUnit.equals("F") && outputUnit.equals("C")){
            output = (temp - 32)  * 5 / 9;
            out.println(temp + " " + inputUnit + " wynosi: " + output  + " " + outputUnit);
        }
        if(inputUnit.equals("K") && outputUnit.equals("F")){
            output = temp * 9 / 5 - 459;
            out.println(temp + " " + inputUnit + " wynosi: " + output + " " + outputUnit);
        }
        if(inputUnit.equals("F") && outputUnit.equals("K")){
            output = (temp - 32)  * 5 / 9 + 273;
            out.println(temp + " " + inputUnit + " wynosi: " + output  + " " + outputUnit);
        }
        if(inputUnit.equals(outputUnit)){
            output = temp;
            out.println(temp + " " + inputUnit + " wynosi: " + output  + " " + outputUnit);
        }
    }
}
