package temperature;

import java.util.function.Function;
import java.util.stream.Stream;

public enum  TemperatureConverter {
    C_TO_K("C", "K", (Float tempToConvert) -> {
        return tempToConvert + 273.15f;
    }),
    C_TO_F("C", "F", (Float tempToConvert) -> {
        return 1.8f*tempToConvert + 32f;
    }),
    K_TO_C("K", "C", (Float tempToConvert) -> {
        return tempToConvert - 273.15f;
    }),
    UNKNOWN("", "", (Float tempToConvert) -> {
        return tempToConvert;
    });

    private String input;
    private String output;
    private Function<Float, Float> convertFunction;

    TemperatureConverter(String input, String output, Function<Float, Float> convertFunction) {
        this.input = input;
        this.output = output;
        this.convertFunction = convertFunction;
    }

    public static float convertTemperature(String input,
                                           String output,
                                           float tempToConvert){
//        for (TemperatureConverter converter : values()){
//            if (converter.input.equals(input) &&
//                    converter.output.equals(output)){
//                return converter.convertFunction.apply(tempToConvert);
//            }
//        }
        //return tempToConvert;
        TemperatureConverter tempConverter = Stream.of(values())
                .filter((TemperatureConverter converter) -> {
                    return converter.input.equals(input) &&
                            converter.output.equals(output);
                })
                .findAny().orElse(TemperatureConverter.UNKNOWN);

        return tempConverter.convertFunction.apply(tempToConvert);
    }
}
