package temperature;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/temp1")
public class TemperatureServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        float temp = Float.parseFloat(req.getParameter("temperature"));
        String input = req.getParameter("input");
        String output = req.getParameter("output");
        float convertTemp = TemperatureConverter.
                convertTemperature(input, output, temp);
//        float convertTemp = 0;
//        if (input.equals("C") && output.equals("K")){
//            convertTemp = temp + 273.15f;
//        }else if (input.equals("C") && output.equals("F")) {
//            convertTemp = 1.8f*temp + 32f;
//        }
        resp.getWriter().println(convertTemp);
    }
}
