package dispatcher;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet("/register")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastName");
        int age = Integer.valueOf(req.getParameter("age"));
        String email = req.getParameter("email");
        String postalCode = req.getParameter("postalCode");
        String pass = req.getParameter("pass");

        if (isFirstCharBig(name) &&
                isFirstCharBig(lastName) &&
                isValidAge(age) &&
                isValidEmail(email) &&
                isValidPostalCode(postalCode) &&
                isValidPass(pass)) {
            req.setAttribute("name", name);
            req.setAttribute("lastName", lastName);
            req.setAttribute("age", age);
            req.setAttribute("email", email);
            req.setAttribute("postalCode", postalCode);
            req.setAttribute("pass", pass);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("welcome.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("register.html");
            resp.setContentType("text/html");
            if (!isFirstCharBig(name))
                resp.getWriter().println("Wrong name!");
            if (!isFirstCharBig(lastName))
                resp.getWriter().println("Wrong last name!");
            if (!isValidAge(age))
                resp.getWriter().println("Wrong age!");
            if (!isValidEmail(email))
                resp.getWriter().println("Wrong email!");
            if (!isValidPostalCode(postalCode))
                resp.getWriter().println("Wrong postal code!");
            if (!isValidPass(pass))
                resp.getWriter().println("Wrong password!");
            requestDispatcher.include(req, resp);
        }
    }

    public boolean isFirstCharBig(String input) {
        return input.charAt(0) >= 'A' && input.charAt(0) <= 'Z';
    }

    public boolean isValidAge(int age) {
        return age > 0 && age < 150;
    }

    public boolean isValidEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public boolean isValidPostalCode(String postalCode) {
        Pattern VALID_POSTAL_CODE = Pattern.compile("^[0-9]{2}-[0-9]{3}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_POSTAL_CODE.matcher(postalCode);
        return matcher.find();
    }
    public boolean isValidPass(String pass) {
        String special1 = "!";
        String special2 = "#";
        return (pass.contains(special1) && pass.contains(special2));
    }
}
