package Przepisy.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@File
public class FileRecipeRepository implements RecipeRepository {

    private Path path;

    public FileRecipeRepository() {
        path = Paths.get("", "recipe.csv");
    }

    @Override
    public void add(Recipe recipe) {

        try {
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
            Files.write(path, RecipeCsvParser.serializeToCSV(recipe).getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Recipe> getAllRecipies() {
        try {
            return Files.readAllLines(path)
                    .stream()
                    .map(RecipeCsvParser::deserializeFromCSV)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public List<Recipe> findByName(String name) {
        return getAllRecipies().stream()
                .filter(recipe -> recipe.getName().contains(name))
                .collect(Collectors.toList());    }

    @Override
    public List<Recipe> findByNumberOfPerson(int number) {
        return getAllRecipies().stream()
                .filter(recipe -> recipe.getAmountOfPersons() == number)
                .collect(Collectors.toList());
    }
}
