package Przepisy.model;

import java.util.List;

public interface RecipeRepository {

    void add(Recipe recipe);

    List<Recipe> getAllRecipies();

    List<Recipe> findByName(String name);

    List<Recipe> findByNumberOfPerson(int number);


}
