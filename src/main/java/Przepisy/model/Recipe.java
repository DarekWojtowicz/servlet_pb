package Przepisy.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Recipe {
   // int ID;
    String name;
    String procedure;
    List<String> ingredients = new ArrayList<>();
    int timeOfDurationInMinutes;
    int calorific;
    int amountOfPersons;

    public Recipe(String name, String procedure, List<String> ingredients, int timeOfDurationInMinutes, int calorific, int amountOfPersons) {

        this.name = name;
        this.procedure = procedure;
        this.ingredients = ingredients;
        this.timeOfDurationInMinutes = timeOfDurationInMinutes;
        this.calorific = calorific;
        this.amountOfPersons = amountOfPersons;
    }

//    public int getID() {
//        return ID;
//    }
//
//    public void setID(int ID) {
//        this.ID = ID;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public int getTimeOfDurationInMinutes() {
        return timeOfDurationInMinutes;
    }

    public void setTimeOfDurationInMinutes(int timeOfDurationInMinutes) {
        this.timeOfDurationInMinutes = timeOfDurationInMinutes;
    }

    public int getCalorific() {
        return calorific;
    }

    public void setCalorific(int calorific) {
        this.calorific = calorific;
    }

    public int getAmountOfPersons() {
        return amountOfPersons;
    }

    public void setAmountOfPersons(int amountOfPersons) {
        this.amountOfPersons = amountOfPersons;
    }

    @Override
    public String toString() {
        return "Recipe{name='" + name + '\'' +
                ", procedure='" + procedure + '\'' +
                ", ingredients=" + ingredients +
                ", timeOfDurationInMinutes=" + timeOfDurationInMinutes +
                ", calorific=" + calorific +
                ", amountOfPersons=" + amountOfPersons +
                '}';
    }

    public static Recipe toObject(String name, String procedure, String ingedients, String calories, String duration, String numberOfPersons){
        String[] data = ingedients.split(",");
        List<String> ingredientsList = new ArrayList(Arrays.asList(data));
        int caloriesInt = Integer.parseInt((calories));
        int durationInt = Integer.parseInt(duration);
        int numberOfPersonsInt = Integer.parseInt(numberOfPersons);
        return new Recipe(name, procedure, ingredientsList, caloriesInt, durationInt, numberOfPersonsInt);
    }
}
