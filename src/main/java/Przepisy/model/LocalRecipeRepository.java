package Przepisy.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Local
public class LocalRecipeRepository implements RecipeRepository {

    private final List<Recipe> reciepies = new ArrayList<>();

    public void add(Recipe recipe) {
        reciepies.add(recipe);
    }

    public List<Recipe> getAllRecipies() {
        return reciepies;
    }

    public List<Recipe> findByName(String name) {
        return reciepies.stream()
                .filter(recipe -> recipe.getName().contains(name))
                .collect(Collectors.toList());
    }

    public List<Recipe> findByNumberOfPerson(int number) {
        return reciepies.stream()
                .filter(recipe -> recipe.getAmountOfPersons() == number)
                .collect(Collectors.toList());
    }
}
