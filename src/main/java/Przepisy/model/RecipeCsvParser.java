package Przepisy.model;

import java.util.Arrays;
import java.util.List;

public class RecipeCsvParser {
    private static final String SEPARATOR = ";";

    public static String serializeToCSV(Recipe recipe){
        return String.format("%s%s%s%s%s%s%s%s%s%s%s\n",
                recipe.getName(),
                SEPARATOR,
                recipe.getProcedure(),
                SEPARATOR,
                recipe.getIngredients(),
                SEPARATOR,
                recipe.getTimeOfDurationInMinutes(),
                SEPARATOR,
                recipe.getCalorific(),
                SEPARATOR,
                recipe.getAmountOfPersons()
                );
    }

    public static Recipe deserializeFromCSV(String data) {
        String[] recipeData = data.split(SEPARATOR);
        List<String> ingredients = Arrays.asList(recipeData[2].replace("[","").replace("]",""));
        return new Recipe(recipeData[0], recipeData[1], ingredients, Integer.parseInt(recipeData[3]), Integer.parseInt(recipeData[4]), Integer.parseInt(recipeData[5]));
    }
}
