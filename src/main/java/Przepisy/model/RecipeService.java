package Przepisy.model;

import javax.inject.Inject;
import java.util.List;

public class RecipeService {
    public static final String NAME_FILTER = "name";
    public static final String PEOPLE_NAME = "people";

    @Inject
    @File
    RecipeRepository recipeRepository;

 //   RecipeRepository recipeRepository = new FileRecipeRepository();

    public void add(Recipe recipe) {
        recipeRepository.add(recipe);
    }

    public List<Recipe> get(String filter, String value) {

        if (filter != null && filter.equals(NAME_FILTER)) {
            return recipeRepository.findByName(value);
        } else if (filter != null && filter.equals(PEOPLE_NAME)) {
            return recipeRepository.findByNumberOfPerson(Integer.valueOf(value));
        } else {
            return recipeRepository.getAllRecipies();
        }
    }
}
