package Przepisy.controler;

import Przepisy.model.Recipe;
import Przepisy.model.RecipeService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/recipe")
public class RecipeServlet extends HttpServlet {

    @Inject
    private RecipeService recipeService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String filter = req.getParameter("filter");
            String value = req.getParameter("value");
            List<Recipe> recipes = recipeService.get(filter, value);
            req.setAttribute("availableRecipes", recipes);
            req.getRequestDispatcher("/RecipeList.jsp")
                    .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String description = req.getParameter("procedure");
        String ingredients = req.getParameter("ingedientsInOneString");
        String calories = req.getParameter("calories");
        String duration = req.getParameter("duration");
        String numberOfPeople = req.getParameter("numberOfPeople");

        Recipe recipe = Recipe.toObject(name, description, ingredients,duration,calories,numberOfPeople);
        recipeService.add(recipe);
        PrintWriter out = resp.getWriter();
        out.println("Przepis dodany: " + recipe);
    }
}
