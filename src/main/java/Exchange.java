import temperature.TemperatureConverter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/kantor")//->http://localhost:8080/kantor
public class Exchange extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        float amount = Float.parseFloat(req.getParameter("amount"));
        String input = req.getParameter("inputCurrency");
        String output = req.getParameter("outputCurrency");
        float convertCurrency = CurrencyConverter.convertCurrency(input, output, amount);
        resp.getWriter().println(convertCurrency);
    }
}